# 网络素养条目
协同过滤：以技术手段利用某兴趣相投、拥有共同经验之群体的喜好来推荐用户感兴趣的信息，个人通过合作的机制给予信息相当程度的回应并记录下来以达到过滤的目的进而帮助别人筛选信息。
网络搜索高级引擎：搜索引擎高级搜索指令是指搜索引擎提供给的一些便于搜索的特殊指令,为了达到用户的直接目的，排除用户不需要的消息。
专注力：是指以专注的意识觉察周遭环境和对自己的思维有所知觉，继而直接塑造我们的思维方式。对自己整体意识和精神状态的关注会创造出一种新的意识，获得控制和管理注意力的能力，能够令我们更好地使用网络，并且这种专注力可以通过训练得以提高。
# ![输入图片说明](https://gitee.com/uploads/images/2017/1122/200424_e8fcbe29_1648203.jpeg "2345_image_file_copy_1.jpg")